# safe_web_api_call_vencabot
## Description
This module provides a decorator function which will call the function repeatedly until it doesn't get a web error. This is meant to ensure that the decorated function will not return until it gets a suitable response.

## Status
It's a very simple script which seems to be fully-working. I may be able to make it more intelligent in the future, such as adding more web errors to handle or even making some error-handling less broad if need be.

## License
This module is licensed under the [GNU General Public License v3.0 or later](https://www.gnu.org/licenses/gpl-3.0-standalone.html).
